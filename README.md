### Hi there 👋
<h1 align="center">Hey, I am Syed Aasim <img src="https://raw.githubusercontent.com/aemmadi/aemmadi/master/wave.gif" width="30px"></h1> 
<h2 align="center"> 🚀 Software Engineer • Front-end Dev • Open Source Enthusiast </h2>


<!--**aasim-syed/aasim-syed** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.-->
 <img src="https://octodex.github.com/images/daftpunktocat-thomas.gif" height="160px" width="160px"> <img src="https://octodex.github.com/images/daftpunktocat-guy.gif" height="160px" width="160px">

Here are some ideas to get you started:

- 🔭 I’m currently contributing to various open -sources
- 🌱 I’m currently learning Express.Js,THREE.Js,SPARK,ANDROID&IOS DEV
- 👯 I’m looking to collaborate on ANYTHING!!!
- 🤔 I’m looking for help with ALL MOST EVERYTHING😂😂

<h3 align="left">Profile Views: 🧐</h3>
<img width="20%" src="https://profile-counter.glitch.me/%7BGitHub-Profile%7D/count.svg" /> 

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=aasim-syed&theme=chartreuse-dark)](https://github.com/anuraghazra/github-readme-stats)
  
<details><summary>Contribution Graph</summary>
<p align="left">
<img width="90%" src="https://activity-graph.herokuapp.com/graph?username=aasim-syed&theme=chartreuse-dark&no-frame=true" /></p>
</details>

  
<details><summary>Trophies</summary>
<p align="left">
<img width=900 src="https://github-profile-trophy.vercel.app/?username=aasim-syed&column=7&theme=gruvbox&no-frame=true"/>
</details>
  

<p align="left">
  <img width="48%" src="https://github-readme-stats.vercel.app/api?username=aasim-syed&show_icons=true&theme=chartreuse-dark&count_private=true&include_all_commits=true" /> 
  <img width="48%" src="https://github-readme-streak-stats.herokuapp.com/?user=aasim-syed&theme=chartreuse-dark" />
</p>  

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://github.com/" target="_blank"> <img src="Images/Github.png" alt="GitHub" width="40" height="40"/>

<a href="https://dev.to/aasimsyed" target="blank"><img align="center" src="Images/dev.jpg" alt="Dev" height="30" width="40" /></a>
<a href="http://www.linkedin.com/in/syed-aasim" target="blank"><img align="center" src="Images/LinkedIn_logo_initials.png" alt="LinkedIn" height="30" width="40" /></a>


<a href="https://www.hackerrank.com/syedaasim133" target="blank"><img align="center" src="Images/HR.png" alt="Hacker Rank" height="30" width="40" /></a>
<a href="https://www.hackerearth.com/@syedaasim133_cs" target="blank"><img align="center" src="Images/hackerearth.png" alt="Hacker Earth" height="30" width="40" /></a>
<a href="https://stackoverflow.com/users/12279947/syed-aasim" target="blank"><img src="Images/Stack_Overflow_icon.svg.png" alt="Stack Overflow" height="30" width="40"></a>
</p>

